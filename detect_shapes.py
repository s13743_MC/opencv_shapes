from shape_detection.shapedetector import ShapeDetector

import sys

sys.path.append('C:\\Users\\Iza\\PycharmProjects\\opecv-test\\venv\\Lib\\site-packages')
import argparse
import imutils
import cv2


def point_max_shape(shapes, shape_name):
    if len(shapes) > 0:
        shape_size_max = 0
        index_of_biggest_shape = 0
        for index_shape, shape in enumerate(shapes):
            if shape[2] > shape_size_max:
                shape_size_max = shape[2]
                index_of_biggest_shape = index_shape
        max_string = "max " + shape_name
        cX = shapes[index_of_biggest_shape][0]
        cY = shapes[index_of_biggest_shape][1]
        cv2.putText(image, max_string, (cX - 20, cY + 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)


# construct the argument parse and parse the arguments
argument_parser = argparse.ArgumentParser()
argument_parser.add_argument("-i", "--image", required=True, help="path to the input image")
args = vars(argument_parser.parse_args())

print(args)

# load the image and resize it to a smaller fator so that
# the shapes can be approximater better
image = cv2.imread(args["image"])
resized = imutils.resize(image, width=300)
ratio = image.shape[0] / float(resized.shape[0])

# convert the resized image to grayscale, blur it slightly, and threshold it
gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(gray, (5, 5), 0)
threshold = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]

# find contours in the thresholded image and initialize the shape detecctor
contours = cv2.findContours(threshold.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
contours = imutils.grab_contours(contours)
shape_detector = ShapeDetector()

global squares
global rectangles
global triangles
global pentagons
global circles

squares = []
rectangles = []
triangles = []
pentagons = []
circles = []

# loop over the contours
for index, contour in enumerate(contours):
    # compute the center of the contour, then detect the name of the
    # shape using only the contour
    M = cv2.moments(contour)
    cX = int((M["m10"] / M["m00"]) * ratio)
    cY = int((M["m01"] / M["m00"]) * ratio)

    shape = shape_detector.detect(contour)
    shape_name = shape[0]
    shape_size = shape[1]

    if shape_name == "square":
        squares.append((cX, cY, shape_size))

    if shape_name == "rectangle":
        rectangles.append((cX, cY, shape_size))

    if shape_name == "triangle":
        triangles.append((cX, cY, shape_size))

    if shape_name == "pentagon":
        pentagons.append((cX, cY, shape_size))

    if shape_name == "circle":
        circles.append((cX, cY, shape_size))

    # multiply the contour (x, y)-coordinates by the resize ratio,
    # then draw the contours and the name of the shape on the image
    contour = contour.astype("float")
    contour *= ratio
    contour = contour.astype("int")
    cv2.drawContours(image, [contour], -1, (0, 255, 0), 2)
    cv2.putText(image, shape_name, (cX + 10, cY), cv2.FONT_HERSHEY_SIMPLEX,
                0.5, (255, 255, 255), 2)

    # draw center point of shape
    cv2.circle(image, (cX, cY), 7, (255, 255, 255), -1)
    # draw size of shape
    cv2.putText(image, str(shape_size), (cX - 20, cY - 20),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)

    if index == 0:
        cv2.putText(image, "Press Enter to calculate next shape", (10, 30),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)

    if index == (len(contours) - 1):
        point_max_shape(squares, "square")
        point_max_shape(rectangles, "rectangle")
        point_max_shape(triangles, "triangle")
        point_max_shape(pentagons, "pentagon")
        point_max_shape(circles, "circle")
        cv2.putText(image, "Press Enter to exit", (10, 50),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)

    # show the output image
    cv2.imshow("Image", image)
    cv2.waitKey(0)
