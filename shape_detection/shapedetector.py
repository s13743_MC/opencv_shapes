import sys

sys.path.append('C:\\Users\\Iza\\PycharmProjects\\opecv-test\\venv\\Lib\\site-packages')
import cv2
import math
import cmath


class ShapeDetector:
    def __init__(self):
        pass

    def detect(self, contour):
        # initialize the shape name and approximate the contour
        shape_name = "unidentified"
        perimeter_of_the_contur = cv2.arcLength(contour, True)
        contour_approximation = cv2.approxPolyDP(contour, 0.04 * perimeter_of_the_contur, True)

        # if the shape is a triangle, it will have 3 vertices
        if len(contour_approximation) == 3:
            shape_name = "triangle"

        # if the shape has 4 vertices, it is either a square or a rectangle
        elif len(contour_approximation) == 4:
            # compute the bounding box of the countour and use the bounding box to compute aspect ratio
            (x, y, w, h) = cv2.boundingRect(contour_approximation)
            aspect_ratio = w / float(h)

            # a square will have an aspect ratio that is approximately equal to one, otherwise, the shape i rectangle
            if aspect_ratio >= 0.95 and aspect_ratio <= 1.05:
                shape_name = "square"
            else:
                shape_name = "rectangle"

        # if the shape is a pentagon, it will have 5 vertices
        elif len(contour_approximation) == 5:
            shape_name = "pentagon"

        # otherwise, we assume the shape is a circle
        else:
            shape_name = "circle"

        print("Shape: " + shape_name)
        list_contours = contour_approximation.tolist()
        print("Amount of vertex coordinates: " + str(len(list_contours)))

        coordinates = []

        for i in range(len(list_contours)):
            x = list_contours[i][0][0]
            y = list_contours[i][0][1]
            print('x' + str(i) + ' = ' + str(x))
            print('y' + str(i) + ' = ' + str(y))

            coordinates.append([x, y])

        print(coordinates)
        shape_size = 0

        if shape_name == "square":
            a = cmath.sqrt(((abs(coordinates[1][0] - coordinates[0][0])) ** 2) + (
                        (abs(coordinates[1][1] - coordinates[0][1])) ** 2))
            shape_size = (a * a).real

        if shape_name == "circle":
            diameter = cmath.sqrt(((abs(coordinates[4][0] - coordinates[0][0])) ** 2) + (
                        (abs(coordinates[4][1] - coordinates[0][1])) ** 2)).real
            radius = diameter / 2
            shape_size = math.pi * radius ** 2

        if shape_name == "triangle":
            a = cmath.sqrt(((abs(coordinates[1][0] - coordinates[0][0])) ** 2) + (
                        (abs(coordinates[1][1] - coordinates[0][1])) ** 2)).real
            b = cmath.sqrt(((abs(coordinates[2][0] - coordinates[1][0])) ** 2) + (
                        (abs(coordinates[2][1] - coordinates[1][1])) ** 2)).real
            c = cmath.sqrt(((abs(coordinates[0][0] - coordinates[2][0])) ** 2) + (
                        (abs(coordinates[0][1] - coordinates[2][1])) ** 2)).real
            # a.real - complex type to float, and int() to integer
            p = (a + b + c) / 2
            shape_size = cmath.sqrt(p * (p - a) * (p - b) * (p - c))

        if shape_name == "rectangle":
            a = cmath.sqrt(((abs(coordinates[1][0] - coordinates[0][0])) ** 2) + (
                    (abs(coordinates[1][1] - coordinates[0][1])) ** 2)).real
            b = cmath.sqrt(((abs(coordinates[2][0] - coordinates[1][0])) ** 2) + (
                    (abs(coordinates[2][1] - coordinates[1][1])) ** 2)).real
            shape_size = a * b

        if shape_name == "pentagon":
            # split pentagon to 3 triangles
            a = cmath.sqrt(((abs(coordinates[1][0] - coordinates[0][0])) ** 2) + (
                        (abs(coordinates[1][1] - coordinates[0][1])) ** 2)).real
            b = cmath.sqrt(((abs(coordinates[2][0] - coordinates[1][0])) ** 2) + (
                        (abs(coordinates[2][1] - coordinates[1][1])) ** 2)).real
            c = cmath.sqrt(((abs(coordinates[0][0] - coordinates[2][0])) ** 2) + (
                        (abs(coordinates[0][1] - coordinates[2][1])) ** 2)).real
            d = cmath.sqrt(((abs(coordinates[1][0] - coordinates[0][0])) ** 2) + (
                        (abs(coordinates[1][1] - coordinates[0][1])) ** 2)).real
            e = cmath.sqrt(((abs(coordinates[2][0] - coordinates[1][0])) ** 2) + (
                        (abs(coordinates[2][1] - coordinates[1][1])) ** 2)).real
            f = cmath.sqrt(((abs(coordinates[0][0] - coordinates[2][0])) ** 2) + (
                        (abs(coordinates[0][1] - coordinates[2][1])) ** 2)).real
            g = cmath.sqrt(((abs(coordinates[0][0] - coordinates[2][0])) ** 2) + (
                        (abs(coordinates[0][1] - coordinates[2][1])) ** 2)).real

            p1 = (a + f + e) / 2
            p2 = (b + c + g) / 2
            p3 = (d + f + g) / 2

            shape_size_p1 = cmath.sqrt(p1 * (p1 - a) * (p1 - f) * (p1 - e)).real
            shape_size_p2 = cmath.sqrt(p2 * (p2 - b) * (p2 - c) * (p2 - g)).real
            shape_size_p3 = cmath.sqrt(p3 * (p3 - d) * (p3 - f) * (p3 - c)).real
            shape_size = shape_size_p1 + shape_size_p2 + shape_size_p3

        print("shape_size after calculation = " + str(int(shape_size.real)))
        print("###################################################")

        # return tuple with a name of the shape and shape size
        return shape_name, int(shape_size.real)